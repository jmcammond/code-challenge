## CMS Developer Challenge
At Trufla we love open source and our client sites are powered mainly by SilverStripe or WordPress.
For this challenge there is no preference given to CMS, pick either to complete the goal. 
This task is about leveraging CSS-based design, points are given to mobile first, semantically correct code.

### Goal
Often we take design and need to convert it to something beautiful. For this task the design and code exist.
Your challenge is to take the static [HTML](http://www.csszengarden.com/examples/index) 
and [CSS](http://www.csszengarden.com/214/214.css) files and create a theme in the CMS of your choice.
Content of the HTML should be managable inside the CMS. Contents of `<nav role="navigation">` should be generated 
from the CMS. Show us your approach for crafting a theme, your build tools and attention to responsive design.

With your completed challenge please include all files, database export and CMS administrator credentials. Also 
include a brief overview of your challenge including highlights, decisions made and bonus items completed.

### Bonus
- Convert CSS to SCSS.
- Create modular content blocks in the CMS editor.
- Add SEO and PageSpeed optimizations.
- Implement a task runner such as Gulp, Grunt or npm.
- Add ability to deploy in a Docker container.

## Challenge Overview
I decided to use [underscores](https://underscores.me/) as a starting point for this theme. I'm also using [Local](https://local.getflywheel.com/) as my development environment. This just made it easier to start building something right away. 

One of the first things that I did when I started on the challenge was to preview the files provided in a browser. After investigating I found that the CSS and html where not really designed for a dynamic content WordPress theme. I decided to use them more as a guide rather than convert the entire CSS file into SCSS.

One of the highlights that I encountered in this challenge was the implementation of ACF. I had never really used it in the past (mostly worked with [MetaBox](https://metabox.io)), so I was surprised how easy it was to include in a theme. I had originally thought about just using widgets for the homepage content but decided against it as it wasn't a very intuitive way to update the content. 

### Admin credentials:
    - username: admin
    - password: admin

### Completed Bonus Items
- Convert CSS to SCSS
- Create modular content blocks in the CMS editor
- Implement a task runner such as Gulp, Grunt, or npm.