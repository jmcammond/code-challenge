<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N/ei6VS7UVQNntzgBTG2XGlpbFyzlWZAlPclUrDQB6mYubOoA6745EG3ubMVfjLqBSuKmtlSlBpA9oWLwBEiPw==');
define('SECURE_AUTH_KEY',  '1RRpxWQ6qontnOukKPDE0Rv50kWV8xBjWYioXob/O4laBu2AZPBd3u5BoOZOHXlHXHTpQkfSD4rh4+tt0FWtYQ==');
define('LOGGED_IN_KEY',    'w1gHEWhVxvTdZCzuz7+A9ath8W3yomfuczH76Wy745bJHmKbuNv3uDFNT+C9BCpf8D4Mpxh5RgZYlIjiOBmFsw==');
define('NONCE_KEY',        'ZntFgWRNA/6+qxwC9iZ/DiiYh2KmAmGXNGqvAVu2BhUpcc6w9ZWL14hpEPf2FIU1ZEIvy+DGaAkvYNk5DPSYZA==');
define('AUTH_SALT',        '+zJIegLgI8caxGdn3k+ktRT4FoKar9Lg19y8RMpmBdUjlUsdx1qAEV0CAweCzU0WaAGjTtiMlD8Q+MxPM6a5QA==');
define('SECURE_AUTH_SALT', 'CYoF+LFAsavXK94uQLpdGAYr+yZiYXU72LuclX9oiZmB7LYhpjyN/BK2yvveVY+jkfE8QynXfUfxKFhJ5XEzdA==');
define('LOGGED_IN_SALT',   'dxyuZrLXX2/J/g9NhVWbaD5jnev16yZitQpopKnaNAL3nH0Z3ycucg3y4acWXRAuZKetAAYtdGBrkaxK8S6G/g==');
define('NONCE_SALT',       'V9ZZNcwtgBJTi5d4RZDfY0TzgqUxKlMlFdr1/fDr3e9+MjlEzE0rJ46Io4Aho5/wgz2+1o4kqApKTL5uVgOOpA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
