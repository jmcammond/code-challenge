<?php
/**
 * Template part for displaying post content in home.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CSSZenGarden
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <article>
        <header class="entry-header">
            <?php echo '<a class="post" href="' . get_permalink($recent["ID"]) . '">' ?>
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            <?php echo '</a>' ?>
        </header><!-- .entry-header -->

        <?php csszengarden_post_thumbnail(); ?>

        <div class="entry-content">
            <?php
            the_excerpt();
            
            wp_link_pages( array(
                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'csszengarden' ),
                'after'  => '</div>',
            ) );
            ?>
        </div><!-- .entry-content -->
    </article>
</div><!-- #post-<?php the_ID(); ?> -->
