<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CSSZenGarden
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php /* Widgets for the top of the content area */ ?>
	<section class="layout l-top">
        <article>
            <?php
                the_content();
            ?>
        </article>
        <aside>
            <ul class="custom-recent-posts">
                <?php
                    $args = array( 'numberposts' => '8' );
                    $recent_posts = wp_get_recent_posts( $args );
                    foreach( $recent_posts as $recent ) {
                        $recent_author = get_user_by( 'ID', $recent["post_author"] );

                        echo '<li>
                            <a class="post" href="' . get_permalink($recent["ID"]) . '">' . $recent["post_title"] . '</a>
                            by
                            <a class="author" href="' . get_author_posts_url($recent["post_author"]) . '">' . $recent_author->display_name . '</a>
                        </li>';
                    }
                    wp_reset_query();
                ?>
            </ul>
        </aside>
    </section>

    <?php /* Widgets for the middle of the content area */ ?>
    <section class="layout l-middle">
        <article>
            <?php the_field('middle_content_section'); ?>
        </article>

        <?php
            if(is_active_sidebar('middle-sidebar')){
                dynamic_sidebar('middle-sidebar');
            }
		?>
    </section>

    <?php /* Widgets for the bottom of the content area */ ?>
    <section class="layout l-bottom">
        <article>
            <?php the_field('bottom_content_section'); ?>
        </article>

        <?php
            if(is_active_sidebar('bottom-sidebar')){
                dynamic_sidebar('bottom-sidebar');
            }
		?>
    </section>
</div><!-- #post-<?php the_ID(); ?> -->
