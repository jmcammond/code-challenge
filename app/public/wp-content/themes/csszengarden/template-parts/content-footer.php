<?php
/**
 * Template part for displaying the footer
 *
 * @package CSSZenGarden
 */

?>

<footer id="colophon" class="site-footer">
    <?php
        if(is_active_sidebar('footer-top')){
            dynamic_sidebar('footer-top');
        }

        if(is_active_sidebar('footer-bottom')){
            dynamic_sidebar('footer-bottom');
        }
    ?>
</footer><!-- #colophon -->