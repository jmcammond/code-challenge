<?php
/**
 * Template part for displaying post content in home.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CSSZenGarden
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<section class="layout l-top">
		<article>
			<header class="entry-header">
				<?php echo '<a class="post" href="' . get_permalink($recent["ID"]) . '">' ?>
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<?php echo '</a>' ?>
			</header><!-- .entry-header -->

			<?php if ( get_edit_post_link() ) : ?>
				<div class="edit-post">
					<?php
						edit_post_link(
							sprintf(
								wp_kses(
									/* translators: %s: Name of current post. Only visible to screen readers */
									__( 'Edit <span class="screen-reader-text">%s</span>', 'csszengarden' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							),
							'<span class="edit-link">',
							'</span>'
						);
					?>
				</div><!-- .entry-footer -->
			<?php endif; ?>

			<?php csszengarden_post_thumbnail(); ?>

			<div class="entry-content">
				<?php
				the_content();
				
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'csszengarden' ),
					'after'  => '</div>',
				) );
				?>
			</div><!-- .entry-content -->
		</article>
		<aside>
            <ul class="custom-recent-posts">
                <?php
                    $args = array( 'numberposts' => '8' );
                    $recent_posts = wp_get_recent_posts( $args );
                    foreach( $recent_posts as $recent ) {
                        $recent_author = get_user_by( 'ID', $recent["post_author"] );

                        echo '<li>
                            <a class="post" href="' . get_permalink($recent["ID"]) . '">' . $recent["post_title"] . '</a>
                            by
                            <a class="author" href="' . get_author_posts_url($recent["post_author"]) . '">' . $recent_author->display_name . '</a>
                        </li>';
                    }
                    wp_reset_query();
                ?>
            </ul>
        </aside>
	</section>

	<?php /* Widgets for the middle of the content area */ ?>
    <section class="layout l-middle">
        <article>
            <?php the_field('middle_content_section'); ?>
        </article>

        <?php
            if(is_active_sidebar('middle-sidebar')){
                dynamic_sidebar('middle-sidebar');
            }
		?>
	</section>
	
	<section class="layout l-bottom">
		<?php if( get_field('bottom_content_section') ): ?>
			<article>
				<?php the_field('bottom_content_section'); ?>
			</article>
		<?php endif; ?>

        <?php
            if(is_active_sidebar('bottom-sidebar')){
                dynamic_sidebar('bottom-sidebar');
            }
		?>
    </section>
</div><!-- #post-<?php the_ID(); ?> -->
