<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CSSZenGarden
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="layout">
				<div class="article-wrapper">
					<?php //article content ?>
					<?php
						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', 'post-home' );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
					?>
				</div>
				
				<div class="sidebar">
					<aside>
						<ul class="custom-recent-posts">
							<?php
								$args = array( 'numberposts' => '8' );
								$recent_posts = wp_get_recent_posts( $args );
								foreach( $recent_posts as $recent ) {
									$recent_author = get_user_by( 'ID', $recent["post_author"] );

									echo '<li>
										<a class="post" href="' . get_permalink($recent["ID"]) . '">' . $recent["post_title"] . '</a>
										by
										<a class="author" href="' . get_author_posts_url($recent["post_author"]) . '">' . $recent_author->display_name . '</a>
									</li>';
								}
								wp_reset_query();
							?>
						</ul>
					</aside>

					<?php
						if(is_active_sidebar('middle-sidebar')){
							dynamic_sidebar('middle-sidebar');
						}
					?>

					<?php
						if(is_active_sidebar('bottom-sidebar')){
							dynamic_sidebar('bottom-sidebar');
						}
					?>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_template_part( 'template-parts/content', 'footer' );
get_sidebar();
get_footer();
