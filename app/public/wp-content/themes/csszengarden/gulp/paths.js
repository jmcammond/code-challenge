module.exports = {
    basePath: "",
    cssBuildPath: "dist/css",
    jsBuildPath: "dist/js",
    imageBuildPath: "dist/image",
    fontBuildPath: "dist/fonts",
    applicationSCSS: [
        "src/scss/style.scss"
    ],
    applicationImages: [
        "src/images/**/*"
    ],
    applicationJS: [
        "src/js/customizer.js",
        "src/js/navigation.js",
        "src/js/skip-link-focus-fix.js"
    ],
    libJS: [

    ],
    libCSS: [

    ],
    libFonts: [
        "src/fonts/**/*.*"
    ]
};