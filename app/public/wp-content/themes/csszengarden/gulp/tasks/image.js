var gulp = require('gulp');
var paths = require('./../paths.js');

gulp.task('build:image', ['clean:images'], function() {
    return gulp.src(paths.applicationImages)
        .pipe(gulp.dest(paths.imageBuildPath));
});