var del = require('del');
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

gulp.task('clean:images', function() {
    return del.sync(['build/image/**', '!build/image']);
});