var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

gulp.task('watch', function() {
    gulp.watch([
        'src/js/**/*.js'
    ], ['script:dev']);
    gulp.watch('src/scss/**/*.scss', ['scss:dev']);
    gulp.watch('src/image/**', ['build:image']);
});