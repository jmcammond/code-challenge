var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var paths = require('../paths.js');
var utils = require('./utils');

/**
 * Compile SCSS into CSS
 */
gulp.task('scss:dev', function() {

    return gulp.src(paths.applicationSCSS)
        .pipe(plugins.sass())
        .on('error', utils.logError)
        .pipe(plugins.minifyCss({processImport: false}))
        .pipe(plugins.rename('build.min.css'))
        .pipe(gulp.dest(paths.cssBuildPath))
        .pipe(plugins.notify(
            function(file) {
                utils.taskComplete({
                    'title': 'Task: scss:dev',
                    'message': 'Application SCSS compiled to - ' + paths.cssBuildPath + '/' + file.relative
                })
            }
        ));

});