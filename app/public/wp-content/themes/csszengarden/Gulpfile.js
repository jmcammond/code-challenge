var gulp = require('gulp');

require('./gulp/tasks/scss');
require('./gulp/tasks/script');
require('./gulp/tasks/watch');
require('./gulp/tasks/lib');
require('./gulp/tasks/image');
require('./gulp/tasks/clean');

/**
 * Default Gulp task
 */
gulp.task('default', ['clean:images', 'script:dev', 'scss:dev', 'lib:dev', 'build:image', 'watch'], function(){
    console.log('Default Finished');
    console.log('Watching...');
});