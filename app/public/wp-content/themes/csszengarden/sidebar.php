<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CSSZenGarden
 */
?>

<section id="secondary" class="sidebar-area">
	<aside>
		<ul class="custom-recent-posts">
			<?php
				$args = array( 'numberposts' => '8' );
				$recent_posts = wp_get_recent_posts( $args );
				foreach( $recent_posts as $recent ) {
					$recent_author = get_user_by( 'ID', $recent["post_author"] );

					echo '<li>
						<a class="post" href="' . get_permalink($recent["ID"]) . '">' . $recent["post_title"] . '</a>
						by
						<a class="author" href="' . get_author_posts_url($recent["post_author"]) . '">' . $recent_author->display_name . '</a>
					</li>';
				}
				wp_reset_query();
			?>
		</ul>
	</aside>

	<?php
		if(is_active_sidebar('middle-sidebar')){
			dynamic_sidebar('middle-sidebar');
		}
	?>

	<?php
		if(is_active_sidebar('bottom-sidebar')){
			dynamic_sidebar('bottom-sidebar');
		}
	?>
</section>