<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CSSZenGarden
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			
			<?php
				if ( have_posts() ) :

					if ( is_home() && ! is_front_page() ) :
						?>
						<header>
							<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
						</header>
						<?php
					endif;

					/* Start the Loop */
					while ( have_posts() ) :
						the_post();
						/*
						* Include the Post-Type-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Type name) and that will be used instead.
						*/
						get_template_part( 'template-parts/content', get_post_type() );
					endwhile;

					the_posts_navigation();

				else :
					get_template_part( 'template-parts/content', 'none' );
				endif;
			?>

			<div class="sidebar">
				<aside>
					<ul class="custom-recent-posts">
						<?php
							$args = array( 'numberposts' => '8' );
							$recent_posts = wp_get_recent_posts( $args );
							foreach( $recent_posts as $recent ) {
								$recent_author = get_user_by( 'ID', $recent["post_author"] );

								echo '<li>
									<a class="post" href="' . get_permalink($recent["ID"]) . '">' . $recent["post_title"] . '</a>
									by
									<a class="author" href="' . get_author_posts_url($recent["post_author"]) . '">' . $recent_author->display_name . '</a>
								</li>';
							}
							wp_reset_query();
						?>
					</ul>
				</aside>

				<?php
					if(is_active_sidebar('middle-sidebar')){
						dynamic_sidebar('middle-sidebar');
					}
				?>

				<?php
					if(is_active_sidebar('bottom-sidebar')){
						dynamic_sidebar('bottom-sidebar');
					}
				?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_template_part( 'template-parts/content', 'footer' );
get_sidebar();
get_footer();
