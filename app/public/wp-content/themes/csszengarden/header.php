<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CSSZenGarden
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div class="site-branding">
			<div class="logo">
				<?php the_custom_logo(); ?>
			</div>
			<div class="title">
				<h1>
					<?php bloginfo('name'); ?>
				</h1>
				<h2>
					<?php 
						$csszengarden_description = get_bloginfo( 'description', 'display' );
						echo $csszengarden_description; 
					?>
				</h2>
			</div>
		</div><!-- .site-branding -->
		<div class="view-all">
			<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">View All Designs</a>
		</div><!-- .view-all -->

		<!-- <nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'csszengarden' ); ?></button>
			<?php /*
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) ); */
			?>
		</nav> -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
